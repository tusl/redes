.. header:: 
  Redes - Unidad 1 - Introducción a los modelos de referencia - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
	PageBreak

Copyright©2016.

:Autor:       M. Celeste Weidmann

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: imagenes/licencia.png
	:scale: 60%

.. raw:: pdf

   PageBreak 



Introducción a los Modelos de Referencia
========================================

Introducción
------------

Importancia de las redes en la actualidad
.........................................

En la actualidad, todo está interconectado; computadoras, teléfonos celulares, tablets, televisores, etc. Vivimos en un tiempo, donde una computadora, por más potente que sea en cuanto a sus recursos, sino tiene una conexión con otros dispositivos, veremos reducida gran parte de su potencial. 
En todas las empresas y oficinas públicas, las computadoras se encuentran interconectadas para compartir recursos e información de una manera eficiente. En nuestras casas, muy probablemente tengamos un equipo que interconecte los dispositivos internos de nuestro hogar con el exterior.

Vivimos en un mundo super conectado, al punto que nos abstraemos de la "magia" que hace posible esa interconexión.
En esta materia trataremos de comprender cómo es posible que una computadora se comunique con otra e intercambie información a miles de kilómetros de distancia en cuestión de milisegundos.


Internet, la red de redes
.........................

Internet es conocida como la Red de redes ya que se trata de un sistema descentralizado de redes de comunicación que conecta a todas las estructuras de redes de ordenadores del mundo. 
Internet es a la vez una oportunidad de difusión mundial, un mecanismo de propagación de la información y un medio de colaboración e interacción entre los individuos y sus ordenadores independientemente de su localización geográfica. [#f1]_ 
Una buena forma de comprender las redes de computadoras, es estudiar la red que navegamos día a día: Internet; Comprender sus orígenes, y el camino transitado para llegar a ser que lo es en la actualidad.


Historia de Internet
....................
Internet se inició en torno al año 1969, cuando el Departamento de Defensa de los EE.UU desarrolló ARPANET, una red de ordenadores creada durante la Guerra Fría cuyo objetivo era eliminar la dependencia de un Ordenador Central, y así hacer mucho menos vulnerables las comunicaciones militares norteamericanas.

A finales de 1969, cuatro ordenadores host fueron conectados cojuntamente a la ARPANET inicial y se hizo realidad una embrionaria Internet.
Se siguieron conectando ordenadores rápidamente a la ARPANET durante los años siguientes y el trabajo continuó para completar un protocolo host a host funcionalmente completo.

En 1972 los científicos de ARPANET demostraron que el sistema era operativo creando una red de 40 puntos conectados en diferentes localizaciones.

Tanto el protocolo de Internet como el de Control de Transmisión fueron desarrollados a partir de 1973, también por el departamento de Defensa norteamericano. 

Cuando en los años 1980 la red dejó de tener interés militar, pasó a otras agencias que vieron en ella interés científico. [#f2]_ 

También a principios de la década del 80 se comenzaron a desarrollar los ordenadores de forma exponencial. El crecimiento era tan veloz que se temía que las redes se bloquearan debido al gran número de usuarios y de información transmitida. La red siguió creciendo exponencialmente.

A partir de la publicación de la tecnología WWW al rededor del año 1991 y junto con la aparición de los primeros navegadores, allá por el año 1993, se comenzó a abrir Internet a un público más amplio: actividades comerciales, páginas personales, etc. Este crecimiento se aceleró con la aparición de nuevos ordenadores más baratos y potentes.


Fundamentos de Redes
--------------------

Una red de computadoras es un conjunto de computadoras autónomas interconectadas mediante una tecnología. Dos computadoras están interconectadas si pueden intercambiar información. La conexión puede llevarse a cabo mediante un cable de cobre, fibra óptica, microondas, infrarrojos o satélites de comunicaciones. 
Las redes pueden ser de muchos tamaños, figuras y formas. Por lo general, las redes se conectan entre sí para formar redes más grandes, en donde Internet es el ejemplo más popular de una red de redes.

¿Por qué son importantes las redes de comunicación?
...................................................

La mayoría de las empresas tienen una cantidad considerable de computadoras, en la mayoría de los casos, las empresas tienen una computadora para cada empleado y la utiliza para realizar su trabajo diario. 

Al principio, algunas de estas computadoras tal vez hayan trabajado aisladas unas de otras, pero en la actualidad, todos los dispositivos de una organización, o al menos, la gran mayoría, necesitan estar conectados a la red de la empresa para distribuir la información. 
El asunto es compartir recursos y la meta es que todos los programas, equipos y en especial los datos estén disponibles para cualquier persona en la red, sin importar la ubicación física del recurso o del usuario. 

Un ejemplo obvio es el de las impresoras; es mucho más eficiente tener una impresora de red a la que todos los empleados de una oficina puedan acceder, que una por empleado. Una impresora en red de alto volumen es más económica, veloz y fácil de mantener que una extensa colección de impresoras individuales.

Aún así, compartir información es más importante que compartir recursos físicos. 


Elementos de uns Sistema de Comunicaciones
..........................................

En todo proceso de comunicación se pueden distinguir una serie de elementos básicos: emisor, receptor, medio o canal, mensaje y codificación.
El emisor es el elemento terminal de la comunicación que se encarga de enviar el mensaje a través de un canal hasta su receptor. 
El receptor es el elemento terminal de la comunicación que recibe la información procedente de un emisor y la interpreta.
La comunicación puede establecerse entre un emisor y un receptor, entre un emisor y múltiples receptores, múltiples emisores y un receptor o bien múltiples emisores y múltiples receptores.
El Medio o Canal es el elemento físico que establece la conexión entre el emisor y el receptor. Puede ser un cable telefónico, cable coaxial,
cable UTP, cable de fibra óptica, medios inalámbricos: ondas electromagnéticas, satélite, ondas de radio.

El Mensaje es la información que se transmite.

La codificación entre equipos debe tener el formato adecuado para el medio. El emisor, primero convierte en bits los mensajes enviados a través de la red. Cada bit se codifica en un patrón de sonidos, ondas de luz o impulsos electrónicos, según el medio de red a través del cual se transmitan los bits. El destino recibe y decodifica las señales para interpretar el mensaje.


.. image:: imagenes/mensaje.png
		:scale: 70%


Tipos de Enlaces
................

Los enlaces de punto a punto conectan pares individuales de máquinas. Para ir del origen al destino en una red formada por enlaces de punto a punto, los mensajes cortos (conocidos como paquetes en ciertos contextos) tal vez tengan primero que visitar una o más máquinas intermedias. A menudo es posible usar varias rutas de distintas longitudes, por lo que es importante encontrar las más adecuadas en las redes de punto a punto.

A la transmisión punto a punto en donde sólo hay un emisor y un receptor se le conoce como unidifusión (unicasting).
Por el contrario, en una red de difusión todas las máquinas en la red comparten el canal de comunicación;   los paquetes que envía una máquina son recibidos por todas las demás. Un campo de dirección dentro de cada paquete especifica a quién se dirige. Cuando una máquina recibe un paquete, verifica el campo de dirección. Si el paquete está destinado a la máquina receptora, ésta procesa el paquete; si el paquete está destinado para otra máquina, sólo lo ignora.

Una red inalámbrica es un ejemplo común de un enlace de difusión, en donde la comunicación se comparte a través de una región de cobertura que depende del canal inalámbrico y de la máquina que va a transmitir.

Por lo general, los sistemas de difusión también brindan la posibilidad de enviar un paquete a todos los destinos mediante el uso de un código especial en el campo de dirección. Cuando se transmite un paquete con este código, todas las máquinas en la red lo reciben y procesan. A este modo de operación se le conoce como difusión (broadcasting). Algunos sistemas de difusión también soportan la transmisión a un subconjunto de máquinas, lo cual se conoce como multidifusión (multicasting).


Protocolos de Comunicación
--------------------------
¿Qué es un Protocolo?
.....................
Un protocolo es un conjunto de reglas perfectamente organizadas y convenidas de mutuo acuerdo entre los participantes en una comunicación y su misión es regular algún aspecto de la misma. 
Es habitual que los protocolos se ofrezcan como normativas o recomendaciones de las asociaciones de estándares. Los fabricantes que se ajustan a estas normativas tienen la seguridad de ser compatibles entre si en aquellos aspectos regulados por el protocolo.

Si no estas familiarizado con las redes de computadoras, quizás el término "protocolo" te resulte un poco fuera de contexto. En comunicaciones, el término protocolo se usa muchísimo, dado que es la base para la comunicación entre dos o más dispositivos, y no es más que un acuerdo entre las partes sobre cómo se debe llevar a cabo la comunicación.
Sería bueno que si no estás familiarizado con este concepto, lo asimiles, porque las redes y computadoras lo utilizan constantemente. Entender como funciona un protocolo en particular, te permitirá encontrar soluciones a problemas de administración de sistemas, además de plantear infraestructuras de servicios de buena calidad.

Tanenbaum lo explica con una analogía: 
"Como una analogía, cuando se presenta una mujer con un hombre, ella podría elegir no darle la mano. Él, a su vez, podría decidir saludarla de mano o de beso, dependiendo, por ejemplo, de si es una abogada americana o una princesa europea en una reunión social formal. Violar el protocolo hará más difícil la comunicación, si no es que imposible."


Concepto de Capa o Nivel en Comunicaciones
..........................................

Con el fin de simplificar la complejidad de su diseño, la mayoría de las redes se organizan como una pila de capas o niveles. Dichas capas están jerarquizadas y cada una está construida a partir de su predecesora. El número de capas, su nombre, el contenido de cada una y su función difieren según el tipo de red. 
Sin embargo, en cualquier red, el propósito de cada capa es ofrecer ciertos servicios a las capas superiores, mientras les oculta los detalles relacionados con la forma en que se implementan los servicios ofrecidos. 
De esta manera, cada capa debe ocuparse exclusivamente de su nivel inmediatamente inferior, a quien solicita servicios, y del nivel inmediatamente superior, a quien devuelve resultados.

Cuando la capa n en una máquina lleva a cabo una conversación con la capa n en otra máquina, a las reglas y convenciones utilizadas en esta conversación se les conoce como el "protocolo de la capa n". En esencia, un protocolo es un acuerdo entre las partes que se comunican para establecer la forma en que se llevará a cabo esa comunicación.

.. image:: imagenes/capas.png
	:scale: 70%


No se transfieren datos de manera directa desde la capa n de una máquina a la capa n de otra máquina, sino que cada capa pasa los datos y la información de control a la capa inmediatamente inferior, hasta que se alcanza a la capa más baja. Debajo de la capa 1 se encuentra el medio físico a través del cual ocurre la comunicación real.

El protocolo en una capa realiza operaciones sobre los datos. Los datos luego pasan a la siguiente capa, donde otro protocolo realiza otras operaciones. En el destino, los protocolos deshacen la construcción del paquete en orden inverso. Los protocolos para cada capa en el destino devuelven la información a su forma original, para que la aplicación pueda leerlos correctamente.

A un conjunto de capas y protocolos se le conoce como arquitectura de red. La especificación de una arquitectura debe contener suficiente información como para permitir que un programador escriba el programa o construya el hardware para cada capa, de manera que se cumpla correctamente el protocolo apropiado.

Lo entenderemos mejor, a medida que avancemos en los modelos de referencia y su estructura.


Modelos de Referencia
---------------------
Analizaremos dos arquitecturas de redes importantes: el modelo de referencia OSI y el modelo de referencia TCP/IP. Aunque ya casi no se utilizan los protocolos asociados con el modelo OSI, el modelo en sí es bastante general y sigue siendo válido; asimismo, las características en cada nivel siguen siendo muy importantes. El modelo TCP/IP tiene las propiedades opuestas: el modelo en sí no se utiliza mucho, pero los protocolos son usados ampliamente.


Modelos de Referencia OSI
.........................

El modelo de interconexión de sistemas abiertos (ISO/IEC 7498-1), más conocido como “modelo OSI”, (en inglés, Open System Interconnection) es un modelo de referencia para los protocolos de la red de arquitectura en capas, creado en el año 1980 por la Organización Internacional de Normalización (ISO, International Organization for Standardization). 
Se ha publicado desde 1983 por la Unión Internacional de Telecomunicaciones (UIT) y, desde 1984, la Organización Internacional de Normalización (ISO) también lo publicó con estándar. [#f3]_

El núcleo de este estándar es el modelo de referencia OSI, una normativa formada por siete capas que define las diferentes fases por las que deben pasar los datos para viajar de un dispositivo a otro sobre una red de comunicaciones. El esquema de este modelo sirvió para la creación de numerosos protocolos.

El advenimiento de protocolos más flexibles donde las capas no están tan desmarcadas y la correspondencia con los niveles no era tan clara puso a este esquema en un segundo plano. Sin embargo se usa en la enseñanza como una manera de mostrar cómo puede estructurarse una "pila" de protocolos de comunicaciones.

Este modelo está dividido en siete (7) capas o niveles:

.. image:: imagenes/modelo_osi.png
	:scale: 70%

La capa Física
~~~~~~~~~~~~~~~
La capa física se relaciona con la transmisión de bits puros a través de un canal de transmisión. Los aspectos del diseño implican asegurarse de que cuando un lado envía un bit 1, éste se reciba en el otro lado como tal, no como bit 0. 
Los aspectos de diseño tienen que ver mucho con interfaces mecánicas, ópticas, eléctricas y de temporización, además del medio físico de transmisión, que está bajo la capa física.
En esta capa se manejan voltajes y pulsos eléctricos, se especifican cables, conectores y componentes de interfaz con el medio de transmisión.
as características que describe la capa física incluye: Niveles de voltaje para representar los bits, duración de un bit, velocidad con que se envían los datos, las distancias máximas de transmisión, si la transmisión puede realizarse en ambos sentidos simultáneamente o no.

La capa de Enlace de datos
~~~~~~~~~~~~~~~~~~~~~~~~~~

La tarea principal de esta capa es transformar un medio de transmisión puro en una línea de comunicación que, al llegar a la capa de red, aparezca libre de errores de transmisión. Logra esta tarea haciendo que el emisor fragmente los datos de entrada en tramas de datos (típicamente, de algunos cientos o miles de bytes) y transmitiendo las tramas de manera secuencial. Si el servicio es confiable, el receptor confirma la recepción correcta de cada trama devolviendo una trama de confirmación de recepción. 

La capa de enlace de datos también se ocupa del direccionamiento físico que permite identificar los hosts origen y destino de la transmisión. También se encarga de resolver el problema provocado por las tramas dañadas producidas por ruido que puede estar presente en el medio físico de transmisión.

Otra cuestión que surge en la capa de enlace de datos es cómo hacer que un transmisor rápido no sature de datos a un receptor lento. Se necesita un mecanismo de regulación de tráfico que indique al transmisor cuánto espacio de búfer tiene el receptor en ese momento. Con frecuencia, esta regulación de flujo y el manejo de errores están integrados. 

Las redes de difusión tienen un aspecto adicional en la capa de enlace de datos: cómo controlar el acceso al canal compartido. 

La capa de Red
~~~~~~~~~~~~~~
Un aspecto clave del diseño es determinar cómo se enrutan los paquetes desde su origen a su destino.
El manejo de la congestión también es responsabilidad de la capa de red, en conjunto con las capas superiores que adaptan la carga que colocan en la red. Otra cuestión más general de la capa de red es la calidad del servicio proporcionado (retardo, tiempo de tránsito, variaciones, etcétera)

Dado que la capa de red debe determinar si el host destino se encuentra en la misma red que el origen o en otra red, necesita de un mecanismo para realizar esta función. Para esto se utilizan direcciones lógicas, en base a las cuales la capa de red puede determinar si envía los datos directamente a través de la red al destino o si debe enviar los datos a través de un dispositivo especial que se encargue de reenviar los datos a través de las redes. Dado que pueden existir múltiples rutas entre el origen y el destino, es misión de los dispositivos de la capa de red determinar las mejores rutas para enviar los datos.

La capa de Transporte
~~~~~~~~~~~~~~~~~~~~~~
La función básica de la capa de transporte es aceptar datos de la capa superior, dividirlos en unidades más pequeñas si es necesario, pasar estos datos a la capa de red y asegurar que todas las piezas lleguen correctamente al otro extremo. 

La capa de transporte también determina el tipo de servicio que debe proveer a la capa de sesión y, en última instancia, a los usuarios de la red. El tipo más popular de conexión de transporte es un canal punto a punto libre de errores que entrega los mensajes o bytes en el orden en el que se enviaron. Sin embargo existen otros posibles tipos de servicio de transporte, como el de mensajes aislados sin garantía sobre el orden de la entrega y la difusión de mensajes a múltiples destinos.

La capa de transporte es una verdadera capa de extremo a extremo; lleva los datos por toda la ruta desde el origen hasta el destino. En otras palabras, un programa en la máquina de origen lleva a cabo una conversación con un programa similar en la máquina de destino mediante el uso de los encabezados en los mensajes y los mensajes de control. En las capas inferiores cada uno de los protocolos está entre una máquina y sus vecinos inmediatos, no entre las verdaderas máquinas de origen y de destino, que pueden estar separadas por muchos enrutadores.

La capa de transporte se encarga de la confiabilidad del transporte de los datos entre los sistemas finales origen y destino, caracterizándose por ser una capa de extremo a extremo. Mientras que en las capas inferiores las comunicaciones se producen entre el host origen y dispositivos vecinos, la capa de transporte se comunica entre los hosts finales que pueden estar separador por muchos dispositivos.

La capa de transporte establece, mantiene y termina adecuadamente los circuitos necesarios para transportar los datos entre los hosts finales. Dado que como se mencionó antes, los datos pueden ser transmitidos por diversos caminos alternativos, el circuito establecido en la capa de transporte se trata de un circuito virtual. Dado que cada computadora puede establecer múltiples conexiones con un mismo sistema final, es necesario contar con un sistema de identificación de cada una de estas conexiones. La capa de transporte provee dicho mecanismo.


La capa de sesión
~~~~~~~~~~~~~~~~~
La capa de sesión permite a los usuarios en distintas máquinas establecer sesiones entre ellos. Las sesiones ofrecen varios servicios, incluyendo el control del diálogo (llevar el control de quién va a transmitir), el manejo de tokens (evitar que dos partes intenten la misma operación crítica al mismo tiempo) y la sincronización (usar puntos de referencia en las transmisiones extensas para reanudar desde el último punto de referencia en caso de una interrupción).


La capa de presentación
~~~~~~~~~~~~~~~~~~~~~~~
A diferencia de las capas inferiores, que se enfocan principalmente en mover los bits de un lado a otro, la capa de presentación se enfoca en la sintaxis y la semántica de la información transmitida.


La capa de aplicación
~~~~~~~~~~~~~~~~~~~~~
La capa de aplicación contiene una variedad de protocolos que los usuarios necesitan con frecuencia. Un protocolo de aplicación muy utilizado es HTTP (Protocolo de Transferencia de Hipertexto, del inglés HyperText Transfer Protocol). Cuando un navegador desea una página web, envía el nombre de la página que quiere al servidor que la hospeda mediante el uso de HTTP. Después el servidor envía la página de vuelta. Hay otros protocolos de aplicación que se utilizan para transferir archivos, enviar y recibir correo electrónico y noticias.

Una característica de esta capa es que no provee servicios a ninguna otra capa del modelo OSI sino a aplicaciones que se encuentran fuera del modelo. 


Modelos de Referencia TCP/IP
............................

El modelo TCP/IP describe un conjunto de guías generales de diseño e implementación de protocolos de red específicos para permitir que un equipo pueda comunicarse en una red. TCP/IP provee conectividad de extremo a extremo especificando como los datos deberían ser formateados, direccionados, transmitidos, enrutados y recibidos por el destinatario.
TCP/IP se ha convertido en el estándar de-facto para la conexión en red. Las redes TCP/IP son ampliamente escalables, para lo que TCP/IP puede utilizarse tanto para redes pequeñas como grandes.

El modelo TCP/IP consta de cuatro (4) capas:

.. image:: imagenes/modelo_tcp_ip.png
	:scale: 70%

La capa de acceso al medio
~~~~~~~~~~~~~~~~~~~~~~~~~~
Es asimilable a la capa 2 (enlace de datos) y a la capa 1 (física) del modelo OSI.


La capa de intered
~~~~~~~~~~~~~~~~~~
Esta capa presenta una correspondencia aproximada a la capa de red del modelo OSI. Su trabajo es permitir que los hosts inyecten paquetes en cualquier red y que viajen de manera independiente hacia el destino (que puede estar en una red distinta). 
Incluso pueden llegar en un orden totalmente diferente al orden en que se enviaron, en cuyo caso es responsabilidad de las capas más altas volver a ordenarlos, si se desea una entrega en orden.

La capa de interred define un paquete de formato y protocolo oficial llamado IP (Protocolo de Internet). El trabajo de la capa de interred es entregar paquetes IP al destinatario. Aquí, el enrutamiento de paquetes es claramente el aspecto principal, con el propósito de evitar la congestión. Por estas razones es  razonable decir que la capa de interred del modelo TCP/IP es similar en funcionalidad a la capa de red del modelo OSI.

La capa de transporte
~~~~~~~~~~~~~~~~~~~~~
La capa que está arriba de la capa de interred en el modelo TCP/IP se llama capa de transporte. Está diseñada para permitir que las entidades iguales en los hosts de origen y destino puedan llevar a cabo una conversación, tal como lo hace la capa de transporte OSI. Aquí se han definido dos protocolos de transporte de extremo a extremo. El primero, TCP (Protocolo de Control de Transmisión), es un protocolo confiable, orientado a la conexión, que permite que un flujo de bytes que se origina en una máquina se entregue sin errores en cualquier otra máquina en la interred.

El segundo protocolo en esta capa, UDP (Protocolo de Datagrama de Usuario) es un protocolo sin conexión, no confiable para aplicaciones que no desean la asignación de secuencia o el control de flujo de TCP y prefieren proveerlos por su cuenta. Se utiliza mucho en las consultas de petición-respuesta de una sola ocasión del tipo cliente-servidor, y en las aplicaciones en las que es más importante una entrega oportuna que una entrega precisa, como en la transmisión de voz o video. UDP no introduce retardos para establecer una conexión, no mantiene estado de conexión alguno y no realiza seguimiento de estos parámetros. 
Con UDP, la transferencia de datos es realizada sin haber realizado previamente una conexión con la maquina de destino, y el destinatario recibirá los datos sin enviar una confirmación al emisor.

La capa de aplicación
~~~~~~~~~~~~~~~~~~~~~
El modelo TCP/IP no tiene capas de sesión o de presentación, ya que no se consideraron necesarias. Las aplicaciones simplemente incluyen cualquier función de sesión y de presentación que requieran. La experiencia con el modelo OSI ha demostrado que esta visión fue correcta: estas capas se utilizan muy poco en la mayoría de las aplicaciones.

Ésta contiene todos los protocolos de alto nivel, como por ejemplo: TELNET, protocolo de transferencia de archivos (FTP), correo electrónico (SMTP), el sistema de nombres de dominio (DNS) para resolución de nombres de hosts a sus direcciones de red; HTTP, el protocolo para recuperar páginas de la World Wide Web; y RTP, el protocolo para transmitir medios en tiempo real.


Encapsulamiento de datos
........................

Para comprender mejor los modelos de referencia, vamos a profundizar un poco en el concepto de encapsulamiento de datos. 
Como estudiamos anteriormente, podemos abstraer un dispositivo como si este fuera un conjunto de capas, donde cada capa se comunica con su capa homónima del otro dispositivo, pero a la vez brinda servicios a las capas adyacentes. 

La comunicación entre dos capas n de distintos dispositivos debe llevarse a cabo utilizando el mismo "tipo de datos". Cuando se envía un mensaje desde el origen hacia el destino, se debe utilizar un formato o estructura específico. Los formatos de los mensajes dependen del tipo de mensaje y el canal que se utilice para entregar el mensaje.

.. image:: imagenes/capas_encapsulacion.png
	:scale: 70%

A medida que los datos atraviesan las capas, cada capa agrega información que posibilita una comunicación eficaz con su correspondiente capa en el otro computador.

Cada capa tiene un PDU (unidad de datos de protocolo) asociada. De modo que cada capa recibe la PDU de la capa superior, y le agrega información que corresponde a sus funciones: direccionamiento, control de errores, etc. 
A continuación lo podemos ver en un gráfico considerando el modelo de referencia OSI. Podemos observar que al conjunto de datos, en el emisor, la capa 7, que es la capa de aplicación le agrega una cabecera de su propia capa para que la interprete la capa 7 del dispositivo receptor (CC7). La capa 6 que es la capa de presentación, toma ese conjunto de datos y le agrega una cabecera propia para que interprete el receptor: CC6 (Cabecera de Capa 6) así sucesivamente hasta llegar a la capa de enlace de datos, la capa 2, que además de agregarle una Cabecera de Capa 2, le agrega una Cola de Capa 2, de esta manera puede saber donde se encuentra el fin de una de determinada trama, ya que para la Capa 1, los datos son bits (ceros y unos).

.. image:: imagenes/encapsulamiento.png
	:scale: 70%

Protocolos por Capas
....................
Como veremos a lo largo de la materia, existen muchísimos protocolos de comunicación, y cada uno está pensado para trabajar en una capa diferente del modelo TCP/IP. A continuación un gráfico donde se puede ver un ejemplo de protocolos y su correspondiente capa.

.. image:: imagenes/protocolos_ejemplos.png
	:scale: 70%

Lecturas obligatorias de la unidad 1:
* Redes de Computadoras. Tanenbaum. Capítulo 1.1 "Uso de las redes de computadoras"  pags. 1 a 14.
* Redes de Computadoras. Tanenbaum. Capítulo 1.3.1 "Jerarquías de Protocolos" pags. 25 a 28.
* Redes de Computadoras. Tanenbaum. Capítulo 1.4 "Modelos de Referencia" pags. 35 a 41.


`Lecturas adicionales:`

`Breve historia de internet <http://www.internetsociety.org/es/breve-historia-de-internet>`_

`La historia de internet <http://www.internetsociety.org/es/breve-historia-de-internet>`_

`El futuro de internet <http://derechoaleer.org/blog/2014/03/un-giro-historico-de-360-grados-para-el-futuro-de-internet.html>`_ 



Bibliografia
=============

* Redes de Computadoras. Andrew S. Tanenbaum, David J. Wetherall. Capítulo 1. 
* https://es.wikipedia.org/wiki/Modelo_OSI
* https://es.wikipedia.org/wiki/Modelo_TCP/IP
* https://es.wikipedia.org/wiki/Historia_de_Internet
* http://www.internetsociety.org/es/breve-historia-de-internet


.. rubric:: Notas al pie
.. [#f1] http://www.maestrosdelweb.com/internethis/
.. [#f2] http://www.ojosdepapel.com/Index.aspx?blog=918
.. [#f3] https://es.wikipedia.org/wiki/Modelo_OSI