.. header:: 
  Redes - Unidad 4 - Direccionamiento IP - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
	PageBreak

Copyright©2016.

:Autor:       M. Celeste Weidmann

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: imagenes/licencia.png
	:scale: 60%

.. raw:: pdf

   PageBreak 




Configuración de RED
====================

Una dirección IP es un número que identifica, de manera lógica y jerárquica, a una Interfaz en red de un dispositivo (computadora, tablet, notebook, smartphone) que utilice el protocolo IP (Internet Protocol), que corresponde al nivel de red del modelo TCP/IP.


Mi configuración
----------------
Para conocer que dirección MAC e IP tiene la computadora a la que estamos conectados, podemos hacerlo a través de la interfaz gráfica o a través de la consola.


A través de la interfaz gráfica
...............................

Para el siguiente ejemplo, usaremos una máquina virtual con Xubuntu, como la que instalaron en la materia: "Ofimatica de las organizaciones".

1) En la barra superior, a la derecha, hacemos clic en el ícono de red

.. image:: imagenes/redes_2.png
	:scale: 90%

2) Luego, para conocer los parámetros de la conexión de red, hacemos clic en "Información de la conexión"

.. image:: imagenes/redes_1.png
	:scale: 90%

3) Veremos un detalle que nos indica: 

* En la sección "General": el tipo de conexión (cableada o Wifi); la dirección MAC señalada como "Dirección hardware"; la velocidad de transmisión, si tiene seguridad o no.

* En la sección "IPv4":  la dirección IP, la dirección de difusión o broadcast, la máscara de subred, el gateway o ruta predeterminada y los datos de DNS.

.. image:: imagenes/redes_3.png
	:scale: 70%

4]) Si quisieramos modificar la configuración de red de nuestra computadora, deberemos hacer clic en el ícono de red de la barra superior del escritorio, luego "Editar conexiones"

.. image:: imagenes/redes_9.png
	:scale: 70%

5) Luego elegimos que conexión queremos editar, en nuestro caso de ejemplo, la conexión cableada. Seleccionamos y hacemos clic en editar

.. image:: imagenes/redes_8.png
	:scale: 70%

6) Luego, seleccionamos la pestaña "Ajustes de IPv4". Si hacemos clic en "metodo" podremos ver distintas maneras de configurar la red, las más comunes son: "DHCP" o "Manual". 
Si seleccionamos manual, deberemos añadir una dirección, y completar con la dirección ip, la máscara de red y la puerta de enlace, además de los servidores DNS..

.. image:: imagenes/redes_7.png
	:scale: 70%


.. image:: imagenes/redes_6.png
	:scale: 70%



A través de la consola
......................

1) Con el comando ifconfig podremos saber las interfaces de red que tenemos configuradas, y la dirección MAC e IP que tiene configurada, entre otras cosas. Podemos conocer los datos de una determinada interfaz de red, con el comando "ifconfig <interfaz>", por ejemplo:

.. code:: bash

    $ ifconfig wlan0


.. image:: imagenes/redes_10.png
	:scale: 90%

2) Podemos modificar la configuración de red editando el archivo: /etc/network/interfaces.

.. code:: bash

    $ nano /etc/network/interfaces

3) Para configurar por dhcp una interfaz de red, deberíamos escribir en el archivo las siguientes lineas (Ejemplo para eth0)

.. image:: imagenes/redes_12.png
	:scale: 90%
 	

4) Para configurar la interfaz como estática o manual, deberiamos escribir en el archivo las siguientes lineas:

.. image:: imagenes/redes_11.png
	:scale: 90%
 	

5) Si modificamos este archivo, luego deberemos reiniciar los servicios de red:

.. code:: bash

    $ /etc/init.d/networking restart

