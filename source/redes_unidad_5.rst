.. header:: 
  Redes - Unidad 5 - Infraestructura de redes - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
	PageBreak

Copyright©2016.

:Autor:       M. Celeste Weidmann

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: imagenes/licencia.png
	:scale: 60%

.. raw:: pdf

   PageBreak 


Cableado estructurado
=====================

Introducción
------------

En los primeros años de la década de los 80’s, los edificios eran diseñados tomando en cuenta muy pocas consideraciones relacionadas con los servicios de comunicaciones que operarían en los mismos. Las compañías de teléfonos instalaban el cable en el momento de la construcción y los sistemas de transmisión de datos se instalaban después de la ocupación del edificio.

El cableado estructurado es un enfoque sistemático del cableado. Es un método para crear un sistema de cableado organizado que pueda ser fácilmente comprendido por los instaladores, administradores de red y cualquier otro técnico que trabaje con cables.
Es el conjunto de elementos pasivos, flexible, genérico e independiente, que sirve para interconectar equipos activos, de diferentes o igual tecnología permitiendo la integración de
los diferentes sistemas de control, comunicación y manejo de la información, sean estos de voz, datos, vídeo, así como equipos de conmutación y otros sistemas de administración.
En un sistema de cableado estructurado, cada estación de trabajo se conecta a un punto central, facilitando la interconexión y la administración del sistema, esta disposición permite la comunicación virtualmente con cualquier dispositivo, en cualquier lugar y en cualquier momento.

Hay tres reglas que ayudan a garantizar la efectividad y eficiencia en los proyectos de diseño del cableado estructurado.

* La primera regla es buscar una solución completa de conectividad. Una solución óptima para lograr la conectividad de redes abarca todos los sistemas que han sido diseñados para conectar, tender, administrar e identificar los cables en los sistemas de cableado estructurado. La implementación basada en estándares está diseñada para admitir tecnologías actuales y futuras. El cumplimiento de los estándares servirá para garantizar el rendimiento y confiabilidad del proyecto a largo plazo.

* La segunda regla es planificar teniendo en cuenta el crecimiento futuro. La cantidad de cables instalados debe satisfacer necesidades futuras. Se deben tener en cuenta las soluciones de Categoría 5e, Categoría 6 y de fibra óptica para garantizar que se satisfagan futuras necesidades. La instalación de la capa física debe poder funcionar durante diez años o más.

* La tercer regla es conservar la libertad de elección de proveedores. Aunque un sistema cerrado y propietario puede resultar más económico en un principio, con el tiempo puede resultar ser mucho más costoso. Con un sistema provisto por un único proveedor y que no cumpla con los estándares, es probable que más tarde sea más difícil realizar traslados, ampliaciones o modificación

Las técnicas de cableado estructurado se aplican en:

* Edificios donde la densidad de puestos informáticos y teléfonos es muy alta: oficinas, centros de enseñanza, tiendas, etc.

* Donde se necesite gran calidad de conexionado así como una rápida y efectiva gestión de la red: Hospitales, Fábricas automatizadas, Centros Oficiales, aeropuertos y terminales.

* Donde a las instalaciones se les exija fiabilidad debido a condiciones extremas: barcos, aviones, estructuras móviles, fábricas que exijan mayor seguridad ante agentes externos.

Algunos conceptos a considerar:

* Los edificios son dinámicos, es decir, durante la existencia de un edificio, las remodelaciones son comunes y deben ser tenidas en cuenta desde el momento del diseño.

* Los sistemas de telecomunicaciones son dinámicos. Durante la existencia de un edificio, las tecnologías y los equipos de telecomunicaciones pueden cambiar drásticamente.

* Telecomunicaciones es más que “Voz y Datos”, ya que el concepto de Telecomunicaciones también incorpora otros sistemas tales como control ambiental, seguridad, audio, televisión, alarmas y sonido.

Estándares
----------

Los estándares son conjuntos de normas o procedimientos de uso generalizado, que se especifican oficialmente, y que sirven como modelo de excelencia. Un proveedor especifica ciertos estándares. Los estándares de la industria admiten la interoperatividad entre varios proveedores de la siguiente forma:

• Descripciones estandarizadas de medios y configuración del cableado backbone y horizontal.

• Interfaces de conexión estándares para la conexión física del equipo.

• Diseño coherente y uniforme que siga un plan de sistema y principios de diseño básicos.

La Asociación de la Industria de las Telecomunicaciones (TIA) y la Asociación de Industrias de Electrónica (EIA) son asociaciones industriales que desarrollan y publican una serie de estándares sobre el cableado estructurado para voz y datos para las LAN.
Tanto la TIA como la EIA están acreditadas por el Instituto Nacional Americano de Normalización (ANSI) para desarrollar estándares voluntarios para la industria de las telecomunicaciones.

ANSI/TIA/EIA-568-B es el estándar para cableado de Telecomunicaciones en Edificios Comerciales. (Cómo instalar el Cableado)

* TIA/EIA 568-B1 Requerimientos generales

* TIA/EIA 568-B2 Componentes de cableado mediante par trenzado balanceado

* TIA/EIA 568-B3 Componentes de cableado, Fibra óptica

ANSI/TIA/EIA-569-A estable las Normas de Recorridos y Espacios de Telecomunicaciones en Edificios Comerciales (Cómo enrutar el cableado)

Partes del cableado estructurado
---------------------------------

El cableado estructurado se puede estudiar considerando sus partes más significantes: 

* Cableado backbone, también conocido como cableado vertical.

* Cableado de distribución, también conocido como cableado horizontal.

* TC: Cuarto de Telecomunicaciones.

* WA: Área de Trabajo.

* ER: Cuarto de Equipos.

* AI: Acometida de Entrada.

* HC: Cruzada horizontal (Cross conexion).

Cableado Backbone
.................
Se distinguen dos tipos de canalizaciones de “back-bone”: Canalizaciones externas, entre edificios y canalizaciones internas al edificio.

Externas
~~~~~~~~
Las canalizaciones externas entre edificios son necesarias para interconectar “Instalaciones de Entrada” de varios edificios de una misma corporación, en ambientes del tipo “campus”. La recomendación ANSI/TIA/EIA-569 admite, para estos casos, cuatro tipos de canalizaciones: Subterráneas, directamente enterradas, aéreas, y en túneles.

Internas
~~~~~~~~
Las canalizaciones internas de “backbone”, son las que vinculan las “instalaciones de entrada” con la “sala de equipos”, y la “sala de equipos” con las “salas de telecomunicaciones”.

* Se utiliza un cableado Multipar UTP y STP , y también, Fibra óptica Multimodo y Monomodo.

* La Distancia Máximas sobre Voz, es de: UTP 800 metros; STP 700 metros; Fibra MM 62.5/125um 2000 metros.

Cableado Horizontal
....................
Se extiende desde el área de trabajo (WA) hasta el armario del cuarto de telecomunicaciones (TC).

Incluye el conector de salida de telecomunicaciones en el área de trabajo (WA), el medio de transmisión empleado para cubrir la distancia hasta el armario, las terminaciones
mecánicas y la conexión cruzada horizontal (HC).

El término “horizontal” se emplea ya que típicamente el cable en esta parte del cableado se instala horizontalmente a lo largo del piso o techo falso.
En el diseño se debe tener en cuenta los servicios y sistemas que se tiene en común: Sistemas de voz y centrales telefónicas, Sistemas de datos, Redes de área local, Sistemas de vídeo, sistemas de seguridad, sistemas de control, etc.

Cableado Horizontal es el cableado que va desde el armario de Telecomunicaciones a la toma de usuario.
La distancia horizontal máxima no debe exceder 90 metros. La distancia se mide desde la terminación mecánica del medio en la interconexión horizontal en el cuarto de telecomunicaciones hasta la toma/conector de telecomunicaciones en el área de trabajo. Además se recomiendan las siguientes distancias: se separan 10 metros para los cables del área de trabajo y los cables del cuarto de telecomunicaciones (cordones de parcheo, jumpers y cables de equipo).

La norma EIA/TIA 568A hace las siguientes recomendaciones en cuanto a la topología del cableado horizontal:

* El cableado horizontal debe seguir una topología estrella. Todos los nodos o estaciones de trabajo se conectan con cable UTP o fibra óptica hacia un concentrador (patch panel) ubicado en el armario de telecomunicaciones de cada piso.

* Cada toma/conector de telecomunicaciones del área de trabajo (WA) debe conectarse a una interconexión en el cuarto de telecomunicaciones (TC).

* No se permiten puentes, derivaciones y empalmes a lo largo de todo el trayecto del cableado.

* Se debe considerar su proximidad con el cableado eléctrico que genera altos niveles de interferencia electromagnética (motores, elevadores, transformadores, etc.) y cuyas limitaciones se encuentran en el estándar ANSI/EIA/TIA 569.

TC: Cuarto de Telecomunicaciones
................................
Las salas de telecomunicaciones (anteriormente “armarios de telecomunicaciones”) se definen como los espacios que actúan como punto de transición entre el Backbone y el cableado de
distribución horizontal. Estas salas generalmente contienen puntos de terminación e interconexión de cableado, equipamiento de control y equipamiento de telecomunicaciones ( típicamente equipos “activos” de datos, como por ejemplo switches). No se recomienda compartir la sala de telecomunicaciones con equipamiento de energía.

La ubicación ideal de la sala de telecomunicaciones es en el centro del área a la que deben prestar servicio. 
Se deben tener en cuenta los requerimientos eléctricos de los equipos de telecomunicaciones que se instalarán en estas salas. En algunos casos, es recomendable disponer de paneles eléctricos propios para las salas de telecomunicaciones. Todas los accesos de las canalizaciones a las salas de telecomunicaciones deben estar selladas con los materiales antifuego adecuados. Es recomendable disponer de ventilación y/o aires acondicionados de acuerdo a las características de los equipos que se instalarán en estas salas.

En muchos casos, en cada piso de un edificio se encuentran armarios de telecomunicaciones y no cuartos de telecomunicaciones.

WA: Área de trabajo 
...................
Los componentes del área de trabajo se extienden desde la terminación del cableado horizontal en la salida de información, hasta el equipo en el cual se está corriendo una aplicación sea de voz, datos, vídeo o control. Normalmente no es de carácter permanente y está diseñado para facilitar los cambios y la reestructuración de los dispositivos conectados.
Son los espacios dónde se ubican los escritorios, boxes, lugares habituales de trabajo, o sitios que requieran equipamiento de telecomunicaciones. 
Las áreas de trabajo incluyen todo lugar al que deba conectarse computadoras, teléfonos, cámaras de vídeo, sistemas de alarmas, impresoras, relojes de personal, etc.

Se recomienda prever como mínimo tres dispositivos de conexión por cada área de trabajo. En base a esto y la capacidad de ampliación prevista se deben prever las dimensiones de las canalizaciones.

ER: Cuarto de Equipos
.....................
Se define como el espacio dónde se ubican los equipos de telecomunicaciones comunes al edificio. Los equipos de esta sala pueden incluir centrales telefónicas (PBX), equipos informáticos (servidores), Centrales de vídeo, etc. Sólo se admiten equipos directamente relacionados con los sistemas de telecomunicaciones.

En el diseño y ubicación de la sala de equipos, se deben considerar:

* Posibilidades de expansión. Es recomendable prever el crecimiento en los equipos que irán ubicados en la sala de equipos, y prever la posibilidad de expansión de la sala.

* Evitar ubicar la sala de equipos en lugar dónde puede haber filtraciones de agua, ya sea por el techo o por las paredes.

* Facilidades de acceso para equipos de gran tamaño.

* Es recomendable que esté ubicada cerca de las canalizaciones “montantes” (back bone), ya que a la sala de equipos llegan generalmente una cantidad considerable de cables desde estas canalizaciones.

AI: Acometida de entrada
.........................
Se define como el lugar en el que ingresan los servicios de telecomunicaciones al edificio y/o dónde llegan las canalizaciones de interconexión con otros edificios de la misma corporación (por ejemplo, si se trata de un “campus”). 
Las “instalaciones de entrada” pueden contener dispositivos de interfaz con las redes publicas prestadoras de servicios de telecomunicaciones, y también equipos de telecomunicaciones. Estas interfaces pueden incluir borneras (por ejemplo telefónicas) y equipos activos (por ejemplo módems). 
El estándar recomienda que la ubicación de las “Instalaciones de entrada” sea un lugar seco, cercano a las canalizaciones de “montantes” verticales (Back-Bone).

HC: Cruzada Horizontal
......................
Elemento usado para terminar y administrar circuitos de comunicación. Se emplean cables de puente (jumper) o de interconexión (patch cord). Existen en cobre y fibra óptica.


La imagen a continuación representa a modo de esquema un edificio con cableado estructurado, donde se pueden observar las partes descriptas anteriormente.

.. image:: imagenes/u5_cableado_estructurado_2.png

Componentes
-----------

Patch panel o patchera
......................
Están formados por un soporte, usualmente metálico y de medidas compatibles con rack de 19”, que sostiene placas de circuito impreso sobre la que se montan: de un lado los conectores
RJ45 y del otro los conectores IDC para block tipo 110. Se proveen en capacidades de 12 a 96 puertos y se pueden apilar para formar capacidades mayores.

Se puede definir como paneles donde se ubican los puertos de una red o extremos (analógicos o digitales) de una red, normalmente localizados en un bastidor o rack de telecomunicaciones. Todas las líneas de entrada y salida de los equipos (computadoras, servidores, impresoras, entre otros) tendrán su conexión a uno de estos paneles.

A continuación, una imagen donde se puede apreciar un patchpanel visto de frente y de atrás, con el detalle de las normas para realizar el cableado.

.. image:: imagenes/u5_patchpanel_10.jpg
		:scale: 120%

En la siguiente imagen, se puede visualizar la forma en que se arma un patch panel:

.. image:: imagenes/u5_patchpanel_42.jpg
		:scale: 60%

Patch core
...........
Es un cable de conexión que se utiliza para conectar un dispositivo electrónico con otro. El cable de red también es conocido como latiguillo de conexión, los instaladores denominan latiguillos a los cables de redes usadas para conectar al usuario final (User Cord) o para conectar equipos dentro del panel de conexiones (Patch Cord). 

Están construidos con cable UTP de 4 pares flexible terminado en un plug 8P8C en cada punta de modo de permitir la conexión entre los 4 pares en un conector RJ45.
A menudo se proveen de distintos colores y con un dispositivo plástico que impide que se curve en la zona donde el cable se aplana al acometer al plug.

Jack
....
Dispositivo modular de conexión monolínea, hembra, apto para conectar plug RJ45, que permite su inserción en rosetas y frentes de patch panels especiales mediante un sistema de encastre. Permite la colocación de la cantidad exacta de conexiones necesarias.

.. image:: imagenes/u5_plug_hembra_1.jpg
		:scale: 60%

Plug/conector RJ45
..................
Los conectores RJ45 es una interfaz física comúnmente usada para conectar redes de cableado estructurado, (categorías 4, 5, 5e, 6 y 6a). Posee ocho pines o conexiones eléctricas, que se usan como extremos de cables de par trenzado.

.. image:: imagenes/u5_plug_1.jpg
	:scale: 60%

.. image:: imagenes/u5_plug_2.jpg
	:scale: 70%

Rack
....
Los Armarios, o Rack de comunicaciones se utilizan para alojar físicamente los elementos que componen los sistemas de cableado es necesario la utilización de armarios rack diseñados exclusivamente para este fin. Dependiendo de la cantidad de elementos a alojar dentro de dichos armarios rack, se ofrecen varias soluciones teniendo en cuenta las necesidades de cada cliente.

.. image:: imagenes/u5_rack.jpg

Rosetas
....... 
Rosetas para jack
~~~~~~~~~~~~~~~~~
Se trata de una pieza plástica de soporte que se arma a la pared y permite encastrar el Jack, el cual se compra por separado.

Rosetas integradas
~~~~~~~~~~~~~~~~~~
Usualmente de 2 bocas, aunque existe también la versión reducida de una boca. Posee un circuito impreso que soporta conectores RJ45 y conectores IDC (Insulation Desplacement Connector) de tipo 110 para conectar los cables UTP sólidos con la herramienta de impacto.

.. image:: imagenes/u5_roseta_1.png
		:scale: 60%

.. image:: imagenes/u5_roseta_2.jpg
		:scale: 50%

Cablecanal
..........
Un cablecanal o canaleta es un canal que contiene cables en una instalación. Las canaletas incluyen conductos comunes de electricidad, bandejas de cables especializadas o bastidores de escalera, sistemas de conductos incorporados en el piso, y canaletas de plástico o metal para montar sobre superficies.

Las canaletas de plástico para montar sobre superficies vienen en varias medidas para acomodar cualquier cantidad de cables. Son más fáciles de instalar que los conductos metálicos.
Existen cable canales o canaletas para piso o para pared.

.. image:: imagenes/u5_canaleta_pared_1.jpg
		:scale: 50%

.. image:: imagenes/u5_canaleta_piso_1.jpg
		:scale: 50%

Herramientas
------------

Pelacables
..........
Las herramientas para pelar cables se usan para cortar el revestimiento de los cables y el aislamiento de los hilos.
Esta herramienta se utiliza para quitar el revestimiento externo de los cables de 4 pares, también se puede utilizar para cables de tipo coaxial. Esta herramienta, por lo general, posee una cuchilla ajustable que sirve para adaptarse a cables con revestimientos de diferentes grosores. El cable se inserta dentro de la herramienta, se gira la herramienta alrededor
del cable. La cuchilla corta sólo el revestimiento, permitiendo que el instalador lo quite, dejando los pares trenzados expuestos.

.. figure:: imagenes/u5_pelacable1.jpg
		:scale: 70%

.. figure:: imagenes/u5_pelacable2.jpg
		:scale: 70%

Impactadoras
............
La herramienta de inserción de impacto sirve para terminar hilos de cable utp en conector jack.

.. image:: imagenes/u5_impactadora_1.jpg
		:scale: 50%


Crimpeadoras
............
Un crimpeadora es una especie de pinza que ejercen una gran presión y sirven para presionar empalmes para los cables de par trenzado.

.. image:: imagenes/u5_crimpeadora_1.png
		:scale: 50%


Verificador de cables UTP
..........................

Sirve para controlar los cableados. Es una herramienta de bajo costo y fácil manejo. Permite detectar fácilmente cables cortados o en cortocircuito, cables corridos de posición, conexiones invertidas, etc.

.. image:: imagenes/u5_cabletester_1.jpg
		:scale: 50%

Estructura de cableado Normas A y B 
-----------------------------------

Los cables UTP son cables de cuatro pares, ocho hilos. Los cables UTP pueden ser derechos o cruzados. Cada uno de estos se utiliza para conectar diferentes tipos de dispositivos.

En los cables derechos o de conexión directa (straight-through), el color del hilo en el pin 1 en un extremo del cable es el mismo que el del pin 1 en el otro extremo. El pin 2 es el mismo que el pin 2 y así sucesivamente. 

.. image:: imagenes/u5_directo_3.png
		:scale: 70%

En los cables cruzados o de "interconexión cruzada" el segundo y el tercer par en un extremo del cable estarán invertidos en el otro extremo. Las salidas de pin serán T568A en un extremo y T568B en el otro. Los 8 conductores (hilos) se deben terminar con conectores modulares RJ-45.

.. image:: imagenes/u5_cruzado.png
		:scale: 70%

En los cables armados según los estándares TIA/EIA T568B o T568A, que determina el color del hilo que corresponde a cada pin. T568B, también denominada de especificación AT&T, es
más común en EE.UU., pero varias instalaciones también se conectan con T568A , también denominado RDSI.


Bibliografia
=============

* https://es.wikipedia.org/wiki/Cableado_estructurado
* https://es.wikipedia.org/wiki/Cable_de_conexi%C3%B3n
* https://es.wikipedia.org/wiki/Rack
* https://es.wikipedia.org/wiki/RJ-45
