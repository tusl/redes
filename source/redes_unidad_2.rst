.. header:: 
  Redes - Unidad 2 - Tipos de Redes - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
	PageBreak

Copyright©2016.

:Autor:       M. Celeste Weidmann

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: imagenes/licencia.png 

.. raw:: pdf

   PageBreak 



Tipos de Redes
==============
Las redes de computadoras pueden clasificarse según su escala. El tamaño de la red es importante como medida de clasificación, ya que las distintas tecnologías se utilizan a diferentes escala. A grandes rasgos, se dividen en: redes de área personal, las cuales están destinadas a una persona, redes de área local, de área metropolitana y de área amplia, cada una con una escala mayor que la anterior.

Clasificación según su escala
-----------------------------

PAN (Personal Area Network) o Redes de Area Personal
....................................................
Las redes de área personal, generalmente llamadas PAN (Personal Area Network) permiten a los dispositivos comunicarse dentro del rango de una persona. Un ejemplo común es una red inalámbrica que conecta a una computadora con sus periféricos. Casi todas las computadoras tienen conectado un monitor, un teclado, un ratón y una impresora. Actualmente, estos dispositivos pueden conectarse a través de una red inalámbrica de corto alcance conocida como Bluetooth. La idea es que si sus dispositivos tienen Bluetooth, no necesitará cables. Sólo hay que ponerlos en el lugar apropiado, encenderlos y trabajarán en conjunto. Para muchas personas, esta facilidad de operación es una gran ventaja.

En su forma más simple, las redes Bluetooth utilizan el paradigma maestro-esclavo, por lo general es el maestro que trata con el ratón, el teclado, etc., como
sus esclavos. El maestro dice a los esclavos qué direcciones usar, cuándo pueden transmitir información, durante cuánto tiempo pueden transmitir, qué frecuencias usar, etcétera.

El alcance de una PAN normalmente se extiende a 10 metros. 

LAN (Local Area Network) o Redes de Área Local
.............................................. 
Una red de área local es un sistema que permite la interconexión de ordenadores que están próximos físicamente, por lo que este tipo de red abarca un área reducida a una casa o una oficina. Son redes de propiedad privada que se encuentran en un solo edificio o como podría ser, un campus universitario.

Se utilizan ampliamente para conectar computadoras personales y estaciones de trabajo en oficinas de una empresa u organización para compartir recursos (por ejemplo, impresoras) e intercambiar información. 

La topología de red define la estructura de una red. Una parte de la definición topológica es la topología física, que es la disposición real de los cables o medios. La otra parte es la topología lógica, que define la forma en que los hosts acceden a los medios para enviar datos.

Las LANs son diferentes de otros tipos de redes en tres aspectos: 1) tamaño; 2) tecnología de transmisión, y 3) topología.

Las LANs están restringidas por tamaño, es decir, el tiempo de transmisión en el peor de los casos es limitado y conocido de antemano. El hecho de conocer este límite permite utilizar ciertos tipos de diseño, lo cual no sería posible de otra manera. Esto también simplifica la administración de la red.


Redes alámbricas e inalámbricas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La topología de muchas redes LAN alámbricas está basada en los enlaces de punto a punto. El estándar IEEE 802.3, comúnmente conocido como Ethernet, es hasta ahora el tipo más común de LAN alámbrica.

En comparación con las redes inalámbricas, las redes LAN alámbricas son mucho mejores en cuanto al rendimiento, ya que es más fácil enviar señales a través de un cable o fibra que por el aire.

VLANS
~~~~~
También es posible dividir una gran LAN física en dos redes LAN lógicas más pequeñas. Esta división es útil en ocasiones donde la distribución del equipo de red no coincide con la estructura de la organización. Por ejemplo, los departamentos de ingeniería y finanzas de una empresa podrían tener computadoras en la misma LAN física debido a que se encuentran en la misma ala del edificio, pero podría ser más sencillo administrar el sistema si cada departamento tuviera su propia red lógica, denominada LAN virtual o VLAN.


MAN (Metropolitan Area Network) o Redes de Área Metropolitana
.............................................................
Una red de área metropolitana (MAN) abarca una ciudad. La interconexión de dos edificios a través de un enlace punto a punto es un buen ejemplo, y es el que se ilustra en la imagen.

.. image:: imagenes/man.png 

El ejemplo más conocido de una MAN es la red de televisión por cable disponible en muchas ciudades. Este sistema creció a partir de los primeros sistemas de antena comunitaria en áreas donde la recepción de la televisión al aire era pobre.

En la ciudad de Santa Fe, la organización "Los Sin Techo" cuenta con una red MAN que interconecta al rededor de 9 salas de computación que se encuentran en los barrios periféricos de la ciudad. Con un punto central con dos antenas, interconecta la ciudad como una especie de red en estrella. Este ejemplo corresponde a una red MAN inalámbrica, o WMAN como también se la llama.

WAN (Area Network) o Redes de Área Amplia
.........................................
Una red de área amplia (WAN), abarca una gran área geográfica, con frecuencia un país o un continente. Contiene un conjunto de máquinas diseñadas para programas (es decir, aplicaciones) de usuario. Las máquinas o hosts están conectados por una subred de comunicación, o simplemente subred. Los clientes son quienes poseen a los hosts (es decir, las computadoras personales de los usuarios), mientras que, por lo general, las compañías telefónicas o los proveedores de servicios de Internet poseen y operan la subred de comunicación. La función de una subred es llevar mensajes de un host a otro, como lo hace el sistema telefónico con las palabras del que habla al que escucha. La separación de los aspectos de la comunicación pura de la red (la subred) de los aspectos de la aplicación (los hosts), simplifica en gran medida todo el diseño de la red.

En la mayoría de las redes de área amplia la subred consta de dos componente distintos: líneas de transmisión y elementos de conmutación. Las líneas de transmisión mueven bits entre máquinas. Pueden estar hechas de cable de cobre, fibra óptica o, incluso, radioenlaces. Los elementos de conmutación son computadoras especializadas que conectan tres o más líneas de transmisión. 
Cuando los datos llegan a una línea de entrada, el elemento de conmutación debe elegir una línea de salida en la cual reenviarlos. Estas computadoras de conmutación reciben varios nombres; conmutadores y enrutadores son los más comunes.


Clasificación según el medio físico
-----------------------------------
Redes Alámbricas o Cableadas
.............................
Son redes donde la comunicación fluye a través de cables de datos (generalmente basada en Ethernet).
Las redes cableadas son la mejor tecnología cuando es necesario mover grandes cantidades de datos a altas velocidades. Una red cableada ofrece mejor rendimiento que una inalámbrica.

Las redes cableadas tienen un costo de instalación mucho más elevado que una red inalámbrica. Se requiere realizar un estudio de instalación, donde se debe contemplar el acceso físico, es decir la arquitectura del edificio que se va a cablear, para montar el cableado estructurado. Se deben considerar varios factores, como la capacidad de crecimiento de la red, es decir, incorporar nuevos puestos de trabajo, y el correspondiente costo asociado para un nuevo tendido.

Redes Inalámbricas
..................
Son redes donde no se requiere el uso de cables de interconexión.
Las redes inalámbricas ofrecen mayor flexibilidad, dado que dentro de la zona de cobertura de la red inalámbrica los nodos se podrán comunicar y no estarán atados a un cable. Además son más fáciles de implementar que las redes cableadas, ya que sólo nos tenemos que preocupar de que el edificio o las oficinas queden dentro del ámbito de cobertura de la red, considerando cuestiones relacionadas a la potencia de transmisión y las interferencias ocasionadas, por ejemplo, por otros dispositivos transmitiendo en el mismo canal.

La transmisión inalámbrica está caracterizada porque utiliza medios no guiados como el aire. Una antena irradia energía electromagnética que es recibida por otra antena. Hay dos métodos para la emisión y recepción de las ondas. En el método direccional, el haz es dirigido en una dirección determinada, por lo que el emisor y el receptor deben estar alineados. 
En el método omnidireccional, las ondas de energía son dispersadas en múltiples direcciones, por lo que varias antenas pueden captarlas.
Para enlaces punto a punto se utilizan frecuencias altas (microondas) y para enlaces multidireccionales con varios receptores se utilizan bajas frecuencias (ondas de radio). Las señales infrarrojas se utilizan para transmisiones en áreas pequeñas, como una habitación.

Un sistema de computadoras portátiles que se comunican por radio se puede considerar una LAN inalámbrica. Este tipo de LAN es un ejemplo de un canal de difusión. Además, tiene propiedades un tanto diferentes que la LAN alámbrica, por lo que requiere distintos protocolos MAC.

Hay un estándar para las redes LAN inalámbricas llamado IEEE 802.11, mejor conocido como WiFi.

Las redes inalámbricas se organizan naturalmente en estas tres configuraciones lógicas: enlaces punto a punto, enlaces punto a multipunto, y nubes multipunto a multipunto.

Punto a punto
~~~~~~~~~~~~~
Red inalámbrica constituida únicamente por dos estaciones, usualmente separadas a una gran distancia. Con antenas apropiadas y existiendo línea visual, se pueden hacer enlaces punto a punto confiables de más de cien kilómetros.

Punto a Multipunto
~~~~~~~~~~~~~~~~~~
Cada vez que tenemos varios nodos hablando con un punto de acceso central estamos en presencia de una red punto a multipunto. El ejemplo típico de un trazado punto a multipunto es el uso de un punto de acceso (Access Point) inalámbrico que provee conexión a varias computadoras portátiles.
Las computadoras portátiles no se comunican directamente unas con otras, pero deben estar en el rango del punto de acceso para poder utilizar la red.


Multipunto a Multipunto
~~~~~~~~~~~~~~~~~~~~~~~
Un enlace multipunto a multipunto, también se denomina red ad-hoc o en malla (mesh). En una red multipunto a multipunto, no hay una autoridad central. Cada nodo de la red transporta el tráfico de tantos otros como sea necesario, y todos los nodos se comunican directamente entre sí.

El beneficio de este diseño de red es que aún si ninguno de los nodos es alcanzable desde el punto de acceso central, igual pueden comunicarse entre sí.
Las buenas implementaciones de redes mesh son auto-reparables, detectan automáticamente problemas de enrutamiento y los corrigen. Extender una red mesh es tan sencillo como agregar más nodos. Si uno de los nodos en la “nube”  tráfico de tantos otros como sea necesario, y todos los nodos se comunican directamente entre sí.

Dos grandes desventajas de esta topología son el aumento de la complejidad y la disminución del rendimiento. La seguridad de esta red también es un tema importante, ya que todos los participantes pueden potencialmente transportar el tráfico de los demás. La resolución de los problemas de las redes multipunto a multipunto tiende a ser complicada, debido al gran número de variables que cambian al moverse los nodos. Las redes multipunto a multipunto generalmente no tienen la misma capacidad que las redes punto a punto, o las punto a multipunto, debido a la sobrecarga adicional de administrar el enrutamiento de la red y el uso más intensivo del espectro de radio.
Sin embargo, las redes mesh son útiles en muchas circunstancias.

Los puntos de acceso tienden a hacer redes punto a multipunto, mientras que los enlaces remotos son punto a punto. Esto implica diferentes tipos de antenas para el propósito.

Clasificación según su topología
---------------------------------
Las redes también se pueden clasificar según la manera en que se conectan las estaciones; es decir, la forma que adopta el medio compartido entre las mismas. Básicamente existen cuatro topologías posibles:

Topología en Estrella
.....................
La topología en estrella conecta todos los nodos con un nodo central. El nodo central conecta directamente con los nodos, enviándoles la información del nodo de origen, constituyendo una red punto a punto. Si falla un nodo, la red sigue funcionando, excepto si falla el nodo central, que las transmisiones quedan interrumpidas.

Se utiliza sobre todo para redes locales. La mayoría de las redes de área local que tienen un enrutador (router), un conmutador (switch) o un concentrador (hub) siguen esta topología. El nodo central en estas sería el enrutador, el conmutador o el concentrador, por el que pasan todos los paquetes. 
Veremos los conceptos de hub, switch y router más adelante en la materia.

.. image:: imagenes/estrella.png

Topología en Bus
................
En la topología de bus todos los nodos están conectados a un circuito común (bus). La topología física de bus consta de un único cable conectando a los dispositivos en un modo serial. Los extremos del cable se terminan con una resistencia que se denomina terminador que permiten cerrar el bus, absorbiendo las señales del cable y no permitiendo que éstas se reflejen al final del cable y se propaguen en dirección contraria a la que la señal tenía originalmente.

En una topología de bus hace posible que todos los dispositivos de la red vean todas las señales de todos los demás dispositivos.

.. image:: imagenes/bus.png

Topología en Anillo
...................
En una topología en anillo, el cable forma un bucle cerrado formando un anillo. Cada dispositivo en esta topología está conectado con dos dispositivos adyacentes, un dispositivo anterior y un dispositivo posterior y los extremos de la topología se conectan entre sí.

.. image:: imagenes/anillo.png

Topología de Malla
..................
En una topología de malla completa, cada nodo perteneciente a la red se enlaza directamente con todos los demás nodos perteneciente a la red. La topología presenta una alta redundancia, ya que cada nodo está directamente conectado a cualquier otro nodo, con lo que si cualquier enlace falla, los datos pueden fluir por alguno de los múltiples enlaces existentes.

Construir una topología física de malla insume una gran cantidad de medios para los enlaces y una gran cantidad de conexiones en los dispositivos.
Debido a los altos costos que insume el desarrollo de una topología en malla, es habitual ver el desarrollo de redes de malla semi-completa (o también denominada irregular) en la cual cada host se conecta con algunos de los dispositivos de la red, pero no con todos, sin seguir un patrón de enlaces y nodos.

.. image:: imagenes/malla.png

Tecnologías de Red
==================

Coaxil
------
El cable coaxial es un medio de transmisión común (conocido simplemente como “coax”). Este cable tiene mejor blindaje y mayor ancho de banda que los pares trenzados sin blindaje, por lo que puede abarcar mayores distancias a velocidades más altas.

Un cable coaxial consiste en alambre de cobre rígido como núcleo, rodeado por un material aislante. 
El aislante está forrado de un conductor cilíndrico, que por lo general es una malla de tejido fuertemente trenzado. El conductor externo está cubierto con una funda protectora de plástico.

Gracias a su construcción y blindaje, el cable coaxial tiene una buena combinación de un alto ancho de banda y una excelente inmunidad al ruido. El ancho de banda posible depende de la calidad y la longitud del cable. Los cables modernos tienen un ancho de banda de hasta unos cuantos GHz. 
Los cables coaxiales solían utilizarse mucho dentro del sistema telefónico para las líneas de larga distancia, pero ahora se reemplazaron en su mayoría por fibra óptica en las rutas de largo recorrido. Sin embargo, el cable coaxial se sigue utilizando mucho para la televisión por cable y las redes de área metropolitana.

Un problema con este tipo de cable son los conectores. Como la malla de cobre que recubre el cable es parte del circuito, ésta debe estar correctamente conectada, garantizando su conexión a tierra. La topología física que se implementa con el cable coaxial es de bus.

Según el proveedor de internet que tengamos en nuestro hogar o en la oficina donde trabajemos, podremos observar como el uso de coaxil, si bien ya no se usa para redes LAN, si se utiliza por los proveedores de internet por cable para la conexión desde el ISP hasta nuestro equipo de comunicación interno que puede ser un modem.

En la imagen a continuación, podemos ver como es por dentro un cable coaxil, identificando las diferentes partes del mismo.

.. image:: imagenes/cable_coaxil.png

En la siguiente imagen, se puede observar como es un conector de cable coaxil.

.. image:: imagenes/conector_coaxil.png

Cable de Par Trenzado
---------------------
Uno de los medios de transmisión más antiguos y todavía el más común es el par trenzado. Un par trenzado consta de dos cables de cobre aislados, por lo general de 1 mm de grosor. Los cables están trenzados en forma helicoidal, justo igual que una molécula de ADN. El trenzado se debe a que dos cables paralelos constituyen una antena simple. Cuando se trenzan los cables, las ondas de distintos trenzados se cancelan y el cable irradia con menos efectividad. Por lo general una señal se transmite como la diferencia en el voltaje entre los dos cables en el par. Esto ofrece una mejor inmunidad al ruido externo, ya que éste tiende a afectar ambos cables en la misma proporción y en consecuencia, el diferencial queda sin modificación.

La aplicación más común del par trenzado es el sistema telefónico. Casi todos los teléfonos se conectan a la central telefónica mediante un par trenzado.

En la materia, llamaremos cable de par trenzado a los cables UTP, que interiormente tiene 4 pares.

Existen diversos tipos de cableado de par trenzado. El que se utiliza con mayor frecuencia en muchos edificios de oficinas se llama cable de categoría 5 o "cat 5", también podemos encontrar fácilmente cables de par trenzado de categoría: 5e o 6. 
Un par trenzado de categoría 5 o 6 consta de dos cables aislados que se trenzan de manera delicada. Por lo general se agrupan cuatro de esos pares en una funda de plástico para protegerlos y mantenerlos juntos. El trenzado de cada uno de los pares es diferente. Cada cable de cobre tiene un cubierta plástica de distinto color, que permite armar cables diferentes siguiendo un código de color.

En la actualidad, el cable UTP es el más popular para la instalación de redes de área local.

.. image:: imagenes/utp_cable_par_trenzado.jpg

Los cables de categoría 5 reemplazaron a los cables de categoría 3 con un cable similar que utiliza el mismo conector, pero tiene más trenzas por metro. Entre más trenzas, hay menos diafonía y se logra una señal de mejor calidad a distancias más largas, lo que hace a los cables más adecuados para la comunicación de computadoras de alta velocidad, en especial para las redes LAN de 100 Mbps y de 1 Gbps.

A los tipos de cables hasta la categoría 6 se les conoce como UTP (Par Trenzado sin Blindaje, del inglés Unshielded Twisted Pair), ya que están constituidos tan sólo de alambres y aislantes. En contraste, los cables de categoría 7 tienen blindaje en cada uno de los pares trenzados por separado, así como alrededor de todo el cable (pero dentro de la funda protectora de plástico). El blindaje reduce la susceptibilidad a interferencias externas y la diafonía con otros cables cercanos para cumplir con las especificaciones más exigentes de rendimiento.

.. image:: imagenes/cable_rj_45.jpg

Fibra Optica
------------
La fibra óptica se utiliza para la transmisión de larga distancia en las redes troncales, las redes LAN de alta velocidad (aunque hasta ahora el cobre siempre ha logrado ponerse a la par) y el acceso a Internet de alta velocidad como FTTH (Fibra para el Hogar, del inglés Fiber To The Home).

A diferencia de los cables de cobre que transportan señales eléctricas, el cable de fibra óptica conduce impulsos de luz moduladas.
La mayor ventaja de la utilización de este tipo de medio es su total inmunidad a todo tipo de interferencias electromagnéticas y alcanza transmisiones de datos a velocidades mayores que los medios de cobre y a distancias más grandes. En contraposición, los costos de la tecnología son más caros.

Un sistema de transmisión óptico tiene tres componentes clave: la fuente de luz, el medio de transmisión y el detector. Por convención, un pulso de luz indica un bit 1 y la ausencia de luz indica un bit 0. El medio de transmisión es una fibra de vidrio ultradelgada. El detector genera un pulso eléctrico cuando la luz incide en él. Al conectar una fuente de luz a un extremo de una fibra óptica y un detector al otro extremo, tenemos un sistema de transmisión de datos unidireccional que acepta una señal eléctrica, la convierte y la transmite mediante pulsos de luz, y después reconvierte la salida a una señal eléctrica en el extremo receptor.

En la imagen que sigue, se puede apreciar un cable de fibra óptica.

.. image:: imagenes/fibra_optica.jpg

Al centro se encuentra el núcleo de vidrio, a través del cual se propaga la luz. El núcleo está rodeado de un revestimiento de vidrio con un índice de refracción más bajo que el del núcleo, con el fin de mantener toda la luz en el núcleo. Después viene una cubierta delgada de plástico para proteger el revestimiento.

.. image:: imagenes/fibra_optica_partes.jpg

Por lo general, las fundas de fibras terrestres se colocan un metro debajo de la superficie, en donde en ocasiones están sujetas a los ataques de retroexcavadoras o topos. Cerca de la costa, las fundas de fibras transoceánicas se entierran en zanjas mediante una especie de arado marino.

A continuación, la imagen muestra un conector de fibra óptica

.. image:: imagenes/conector_cable_fibra.jpg

Por lo general se utilizan dos tipos de fuentes de luz para producir las señales: LED (Diodos Emisores de Luz, del inglés Light Emitting Diodes) y láseres semiconductores. Estas fuentes de luz tienen distintas propiedades.

Comparación entre cable de cobre y fibra
----------------------------------------
La fibra tiene muchas ventajas. Para empezar, puede manejar anchos de banda mucho mayores que el cobre. Tan sólo por esto sería indispensable en las redes de alto rendimiento. Debido a la baja atenuación, sólo se necesitan repetidores aproximadamente cada 50 km en líneas extensas, mientras que el cobre requiere repetidores cada 5 km, lo cual implica un ahorro considerable en el costo. 
La fibra también tiene la ventaja de que no le afectan las sobrecargas de energía, la interferencia electromagnética ni los cortes en el suministro de energía. Tampoco le afectan las sustancias corrosivas en el aire, lo cual es importante en los ambientes industriales pesados.


`Lecturas adicionales de la unidad 2:`

`Redes de Computadoras`_  Capítulo 1.2 "Hardware de Red"  pags. 15 a 23.
`Redes de Computadoras`_  Capítulo 2.2 "Medios de Transmisión Guiados"  pags. 82 a 87.


Bibliografia
=============
* Redes de Computadoras. Andrew S. Tanenbaum, David J. Wetherall.
* Redes Inalámbricas para países en desarrollo. http://wndw.net/
* https://es.wikipedia.org/wiki/Red_de_%C3%A1rea_local
* https://es.wikipedia.org/wiki/Red_de_%C3%A1rea_metropolitana
* https://es.wikipedia.org/wiki/Red_de_%C3%A1rea_amplia
* https://es.wikipedia.org/wiki/Cable_de_par_trenzado
* https://es.wikipedia.org/wiki/WPAN

