================================================
Software Libre: Redes 
================================================
--------------------------------------------------------------------
Trabajo Práctico Final - 2016
--------------------------------------------------------------------

.. header::
  Software Libre: Redes- Trabajo Práctico Final

.. footer::
  Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2016 

:Autor: María Celeste Weidmann
:Autor: Maximiliano Rossier



.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: imagenes/licencia.png

.. raw:: pdf

    PageBreak

Trabajo Práctico Final - 2016
------------------------------------------------

**Fecha de entrega: 31/07/2016**


**Pautas para la presentación del Trabajo Práctico**

    - En formato de impresión A4.
    - En un formato de archivo abierto (se recomienda odt o pdf).
    - Se deberá realizar y subir el trabajo en el aula de manera individual, en el Plan de Trabajo correspondiente.
    - Con carátula conteniendo:
       a. Rótulo identificatorio de la Unidad Académica, y la materia: UNLVirtual - Educación a Distancia Universidad Nacional del Litoral - Redes
       b. Título del Trabajo Práctico y nombre del alumno.
       c. Año de cursado.
       d. Deberán seleccionar una de las licencias Creative Commons (http://www.creativecommons.org.ar/licencias).
    - Cada hoja del trabajo práctico debe contener:
       a. Cabecera de página o encabezado: Título del Trabajo Práctico y materia
       b. Pie de página: Nombre y Apellido
    - El nombre del archivo debe tener el siguiente formato: TP_FINAL_Apellido_Nombre.pdf o TP_FINAL_Apellido_Nombre.odt
    - Las imágenes no deben superar los 10,0cm de alto.
    - Aplique los siguientes margenes al documento:
       a. Superior: 3,0cm
       b. Inferior: 2,0cm
       c. Izquierdo: 3,0cm
       d. Derecho: 2,0cm
    - El título debe tener fuente Nimbus Sans L o Arial de 14pts, Negrita, subrayado, centrado, separado al párrafo siguiente por 0,5cm.
    - Los subtítulos deben estar en fuente Nimbus Sans L o Arial 12pts, Negrita Cursiva, párrafo alineado centrado y con separación del párrafo superior de 0,8cm y del párrafo inferior de 0,6cm. 
    - Los párrafos de texto normales deben estar en fuente Nimbus Sans L o Arial 10pts, párrafos justificados, interlineado simple, con sangría del tipo de que la primer linea de cada párrafo comience a 2,0cm del margen izquierdo y con separación del párrafo superior de 0,2cm y del párrafo inferior de 0,2cm.



.. raw:: pdf

    PageBreak

**Criterios de evaluación:**

    - Respetar las pautas enunciadas.
    - Responder las consignas.
    - En caso de ser necesario, citar a los autores.

**Enunciado:**

Una empresa dedicada a las TIC's va a implementar un cableado estructurado en su nueva oficina de nuestra ciudad. Para lo cual nos han contratado para que verifiquemos, implementemos y certifiquemos el plan que desean para dicho trabajo.

En el plano que se adjunta al final del documento se especifica el cableado estructurado para las pc en los puestos de trabajo e impresoras (color VERDE) y el de los servidores (color CELESTE) del centro de datos de la empresa.

Nos solicitan que evaluemos el plan propuesto para corroborrar una correcta implementación.

Determinar y proponer:

1. De acuerdo a lo estudiado en la unidad 2,  Clasifique la red a implementar según el plano adjunto. La idea es que pueda clasificar la red según la escala, según el medio físico y su topología.

2. En la Unidad 2 también estudiamos las tecnologías de red, ¿Cuál considera más adecuada para esta implementación y por que? (Coaxil, Par trenzado, Fibra óptica)

3. Según lo estudiado en la unidad 3 ¿Qué tipo de dispositivos de comunicación debe colocar y dónde los ubicaría?

4. Considerando que el cableado, además de contemplar el cableado para cada impresora de red detallada en el plano, debe contemplar una PC por cada escritorio gris, y que cada puesto de trabajo debe contener para este caso, 2 bocas de red, indique:

a. Cantidad de rosetas integradas de 2 bocas a utilizar.
b. Cantidad de patch panel de 48 bocas sólo para conectar PC e Impresoras. Recuerde que tiene dos bocas por cada PC.
c. Cantidad de RACK o Armarios a utilizar.

5. De acuerdo a lo estudiado en la unidad 4, ¿Qué cantidad de direcciones ip necesitaría para direccionar los host y las impresoras (Considere solamente las PC de escritorio, no los servidores). 

6. Teniendo en cuenta que la red de Servidores, la red de pc e impresoras de Administración, la red de pc e impresoras de Atención al Público y la red de la Sala de Capacitaciones tienen que ser diferentes, ¿que mecanismo utilizaría para tal fin? ¿Redes diferentes, subredes, VLANs?

7. Según lo estudiado en la unidad 5, Indique en el plano las partes de cableado estructurado que se encuentran visibles: AW, TC, Cableado de distribución, AI.

8. Realizar una conclusión final personal sobre los conceptos y la experiencia adquirida durante la materia y lo visto en el trabajo práctico.